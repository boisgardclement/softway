# Softway



## Run application

- sudo apt-get install -y -qq maven openjdk-11-jdk
- mvn package
- java -cp target/softwayEntry-1.0.jar com.mycompany.app.App

## Run lint

- wget --quiet --output-document=pmd.zip https://github.com/pmd/pmd/releases/download/pmd_releases%2F6.55.0/pmd-bin-6.55.0.zip
- unzip pmd.zip
- ./pmd-bin-6.55.0/bin/run.sh pmd -d ./ --fail-on-violation false -f html -R ruleset.xml > lint_pmd.html

# version of dependencies

Machine:
- WSL 2
- Linux 5.10.16 Ubuntu 20.04.5 LTS

Software:
- Apache Maven 3.6.3
- Junit 4.11

Lint:
- PMD 6.55