package com.mycompany.app;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
@SuppressWarnings({"PMD.AtLeastOneConstructor", "PMD.SystemPrintln", "PMD.CommentRequired"})
public class AppTest
{
    private final PathologyAnalyser pathologyAnalyser = new PathologyAnalyser(); //NOPMD
    private final static String NOPATHOLOGY = "No pathology";

    @Test
    public void returnFracture()
    {
        pathologyAnalyser.createPathologyString(5);

        assertEquals("Value 5 should give:", "FRACTURE", pathologyAnalyser.getPathologyString());
    }

    @Test
    public void returnCardiaque()
    {
        pathologyAnalyser.createPathologyString(3);
    
        assertEquals("Value 3 should give:", "CARDIAQUE", pathologyAnalyser.getPathologyString());
    }

    @Test
    public void multipleReturn()
    {
        pathologyAnalyser.createPathologyString(15);
    
        assertEquals("Value 15 should give:",  "FRACTURE, CARDIAQUE", pathologyAnalyser.getPathologyString());
    }

    @Test
    public void notARealPathology()
    {
        pathologyAnalyser.createPathologyString(8);
    
        assertEquals("Value 8 should give:", NOPATHOLOGY, pathologyAnalyser.getPathologyString());
    }

    @Test
    public void specialCaseZero()
    {
        pathologyAnalyser.createPathologyString(0);
    
        assertEquals("Value 0 should give:", NOPATHOLOGY, pathologyAnalyser.getPathologyString());
    }

    @Test
    public void specialCaseMaxInteger()
    {
        pathologyAnalyser.createPathologyString(Integer.MAX_VALUE);
    
        assertEquals("Value MAX_VALUE should give:", NOPATHOLOGY, pathologyAnalyser.getPathologyString());
    }

    @Test
    public void specialCaseMinInteger()
    {
        pathologyAnalyser.createPathologyString(Integer.MIN_VALUE);
    
        assertEquals("Value MIN_VALUE should give:", NOPATHOLOGY, pathologyAnalyser.getPathologyString());
    }
}
