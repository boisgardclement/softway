package com.mycompany.app;

import java.util.Scanner;

/**
* Description: The main class
* @author Boisgard Clement
*/
@SuppressWarnings({"PMD.ShortClassName", "PMD.SystemPrintln"})
public class App {

    /**
    * Description: The main function, read the input and give the pathology
    * @author Boisgard Clement
    */
    public static void main(final String[] args) {
        final PathologyAnalyser pathologyAnalyser = new PathologyAnalyser();
        final Scanner scannerInput = new Scanner(System.in); //NOPMD

        while (true) {
            System.out.println("Enter an integer number:");
            final String scannedString = scannerInput.nextLine();
            int scannedInteger;

            try {  
                scannedInteger = Integer.parseInt(scannedString); 
            } catch(NumberFormatException e) {  
                System.out.println("Is not an integer number, try again !");
                continue;
            }  
            
            pathologyAnalyser.createPathologyString(scannedInteger);
            System.out.println(pathologyAnalyser.getPathologyString());
        }
    }
}