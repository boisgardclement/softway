package com.mycompany.app;

import java.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;

/**
* Description: 
* This class provide the analysis of the health number
* according to a dictionnary of pathology.
* 
* @author Boisgard Clement
*/
@SuppressWarnings({"PMD.DoNotTerminateVM", "PMD.SystemPrintln", "PMD.CommentRequired"})
public class PathologyAnalyser {

    private String pathologyString = "Nothing is computed";
    private Map<String, Integer> pathologyDict;
    private final static String NOPATHOLOGY = "No pathology";
    private final static String PATHOLOGYDICTPATH = "pathologyDict.json";

    public PathologyAnalyser() {
        final ObjectMapper objectMapper = new ObjectMapper();
        final File jsonFile = new File(PATHOLOGYDICTPATH);

        try {
            pathologyDict = objectMapper.readValue(jsonFile, Map.class);
        } catch (IOException e) {
            e.printStackTrace(); //NOPMD
            System.exit(-1);
        }
    }

    /**
    * @return The string to display with the pathology detected
    * @author Boisgard Clement
    */
    public String getPathologyString() {
        return this.pathologyString;
    }

    /**
    * Description: Compute the diagnostic
    * @param healthIndex The index number of the diagnostic
    * @author Boisgard Clement
    */
    public void createPathologyString(final int healthIndex) {
        final List<String> pathology = new ArrayList<>();

        // Test if the dictionnary is valid
        if (!isDictionnaryValid(pathologyDict)) {
            System.err.println("Error in the dictionnary, could not continue.");
            System.exit(-1);
        }

        // Iterate over each pathology and test if present
        for (final String key: pathologyDict.keySet()) {
            if (isPathologyPresent(healthIndex, pathologyDict.get(key))) {
                pathology.add(key);
            }
        }

        // In case their is no pathology
        if (pathology.isEmpty()) {
            this.pathologyString = NOPATHOLOGY;
        } else {
            // Transform the pathology array into string join with comma
            this.pathologyString = String.join(", ", pathology);
        }
    }

    /**
    * @param healthIndex The index number of the diagnostic
    * @param pathology The number that represente the pathology
    * @return The string to display with the pathology detected
    * @author Boisgard Clement
    */
    private boolean isPathologyPresent(final int healthIndex, final int pathology) {
        // Special case for the 0 value or negative
        if (healthIndex <= 0) {
            return false;
        }

        return healthIndex % pathology == 0;
    }

    /**
    * @param pathologyDict The dictionnary of pathology
    * @return The validity of the dictionnary of pathology
    * @author Boisgard Clement
    */
    private boolean isDictionnaryValid(final Map<String, Integer> pathologyDict) {
        final List<Integer> values = new ArrayList<>(pathologyDict.values()); //NOPMD
        final List<String> keys = new ArrayList<>(pathologyDict.keySet()); //NOPMD

        // rule 0: The dictionnary must not be empty
        if (!isDictionnaryRule0Valid(pathologyDict)) {
            return false;
        }

        // rule 1: The dictionnary key and value must be unique
        if (!isDictionnaryRule1Valid(keys, values)) {
            return false;
        }
        
        // rule 2: The dictionnary key must be primary between them
        if (!isDictionnaryRule2Valid(values)) { //NOPMD
            return false;
        }

        return true;
    }

    /**
    * @param pathologyDict The dictionnary of pathology
    * @return The dictionnary must not be empty
    * @author Boisgard Clement
    */
    private boolean isDictionnaryRule0Valid(final Map<String, Integer> pathologyDict) {
        return !pathologyDict.isEmpty();
    }

    /**
    * @param keys The dictionnary of pathology keys
    * @param values The dictionnary of pathology values
    * @return The dictionnary key and value must be unique
    * @author Boisgard Clement
    */
    private boolean isDictionnaryRule1Valid(final List<String> keys, final List<Integer> values) {
        final Set<String> uniqueKey = new HashSet<>(keys);
        final Set<Integer> uniqueValues = new HashSet<>(values);

        return keys.size() == uniqueKey.size() && values.size() == uniqueValues.size();
    }

    /**
    * @param values The dictionnary of pathology values
    * @return The dictionnary key and value must be unique
    * @author Boisgard Clement
    */
    private boolean isDictionnaryRule2Valid(final List<Integer> values) {
        // Sort element
        Collections.sort(values);

        // Check each element if he could divise each other
        for (int i = 0; i < values.size() - 1; i++) {
            for (int j = i+1; j < values.size(); j++) {
                if (values.get(j) % values.get(i) == 0) {
                    return false;
                }
            }
        }

        return true;
    }
}